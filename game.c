#include "pishtov.h"
#include "arr.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>

#define PI 3.141592653589793238
#define E  2.718281828459045235

#define MIN_NOISE_RES 8
#define MAX_NOISE_RES 128

#define MAX_PATH_GENERATIONS_PER_TICK 1
#define GUYS_LEN 90000

#define SIGN(X) (((X) > 0) - ((X) < 0))

#define WATER_TRESHOLD .1

size_t field_w = 300;
size_t field_h = 200;

float ticks_per_second = 20.f;
float seconds_since_last_tick = 0;

float min_movement_cost = .01f;
float road_decay_rate = .999;

typedef struct Position Position;
struct Position {
    int64_t x;
    int64_t y;
};

typedef struct Guy Guy;
struct Guy {
    int64_t path_idx;
    struct Position *path;
};

typedef struct A_Star_Element A_Star_Element;
struct A_Star_Element {
    A_Star_Element *prev;
    struct Position pos;
    float cost;
    float predicted_cost;
    int64_t path_len;
};

float *field;
float *roads;
Guy guys[GUYS_LEN];

static unsigned long xorshf_x=123456789, xorshf_y=362436069, xorshf_z=521288629;

uint64_t get_timestamp() {
    struct timespec ts;
    timespec_get(&ts, TIME_UTC);
    return ts.tv_sec * 1000000000 + ts.tv_nsec;
}

// xorshf96
// period 2^96-1
uint64_t rand64() {
    uint64_t t;
    xorshf_x ^= xorshf_x << 16;
    xorshf_x ^= xorshf_x >> 5;
    xorshf_x ^= xorshf_x << 1;

    t = xorshf_x;
    xorshf_x = xorshf_y;
    xorshf_y = xorshf_z;
    xorshf_z = t ^ xorshf_x ^ xorshf_y;

    return xorshf_z;
}

void srand64(uint64_t seed) {
    seed &= 0x0fffff;
    xorshf_x=123456789, xorshf_y=362436069, xorshf_z=521288629;
    for (uint64_t i = 0; i < seed; ++i) rand64();
}

float frand() {
    return (rand64() & 0xffffff) / 16777216.f;
}

void generate_random_scalars(float *buf, size_t w, size_t h) {
    for (size_t i = 0; i < h; ++i) {
        for (size_t j = 0; j < w; ++j) {
            buf[i * w + j] = frand() * 2.f - 1.f;
        }
    }
}

float dot(float x1, float y1, float x2, float y2) {
    return x1 * x2 + y1 * y2;
}

float vec_cos(float x1, float y1, float x2, float y2) {
    const float d1 = sqrt(x1 * x1 + y1 * y1);
    const float d2 = sqrt(x2 * x2 + y2 * y2);
    return dot(x1 / d1, y1 / d1, x2 / d2, y2 / d2);
}

float blerp(float tl, float tr, float bl, float br, float x, float y) {
    const float X = 1.f - x;
    const float Y = 1.f - y;
    return
        tl * X * Y + tr * x * Y +
        bl * X * y + br * x * y;
}

void generate_noise(float *buf, size_t w, size_t h) {
    float *scalars = malloc(MAX_NOISE_RES * MAX_NOISE_RES * 2 * sizeof(*scalars));

    for (size_t i = 0; i < h; ++i) {
        for (size_t j = 0; j < w; ++j) {
            buf[i * w + j] = 0;
        }
    }

    for (size_t res = MIN_NOISE_RES; res <= MAX_NOISE_RES; res *= 2) {
        generate_random_scalars(scalars, res, res);
        const float mul = ((float)MIN_NOISE_RES / res * .5f);

        for (size_t i = 0; i < h; ++i) {
            for (size_t j = 0; j < w; ++j) {
                const float x = ((float)j / (float)w + 1.f / (w + 1.f)) * (res - 1.f);
                const float y = ((float)i / (float)h + 1.f / (h + 1.f)) * (res - 1.f);

                const size_t x0 = floor(x);
                const size_t y0 = floor(y);
                const size_t x1 = x0 + 1;
                const size_t y1 = y0 + 1;

                const float tl = scalars[y0 * res + x0];
                const float tr = scalars[y0 * res + x1];
                const float bl = scalars[y1 * res + x0];
                const float br = scalars[y1 * res + x1];

                buf[i * w + j] += blerp(tl, tr, bl, br, x - x0, y - y0) * mul;
            }
        }
    }

    free(scalars);
}

bool a_star_cmp(A_Star_Element *a, A_Star_Element *b) {
    return a->cost + a->predicted_cost < b->cost + b->predicted_cost;
}

A_Star_Element **a_star_create() {
    A_Star_Element **ret = arr_create(A_Star_Element*);
    arr_resize(&ret, 1);
    return ret;
}

void a_star_free(A_Star_Element **heap) {
    arr_free(heap);
}

void a_star_push(A_Star_Element ***heap, A_Star_Element *e) {
    arr_push(heap, e);
    for (int64_t i = arr_len(*heap) - 1; i > 1; i /= 2) {
        if (a_star_cmp((*heap)[i], (*heap)[i/2])) {
            A_Star_Element *tmp;
            tmp          = (*heap)[i];
            (*heap)[i]   = (*heap)[i/2];
            (*heap)[i/2] = tmp;
        }
    }
}

A_Star_Element *a_star_pop(A_Star_Element ***heap) {
    A_Star_Element *ret = (*heap)[1];

    (*heap)[1] = arr_pop(heap);

    for (int64_t i = 1; i * 2 < arr_len(*heap);) {
        int64_t j = i * 2;
        if (j + 1 < arr_len(*heap) && a_star_cmp((*heap)[j + 1], (*heap)[j])) j = j + 1;
        if (a_star_cmp((*heap)[i], (*heap)[j])) break;

        A_Star_Element *tmp;
        tmp        = (*heap)[i];
        (*heap)[i] = (*heap)[j];
        (*heap)[j] = tmp;

        i = j;
    }

    return ret;
}

bool a_star_empty(A_Star_Element **heap) {
    return arr_len(heap) <= 1;
}

// TODO this is dijkstra not a*
Position *a_star(Position start, Position end) {
    A_Star_Element **heap = a_star_create();

    A_Star_Element *a_star_elements = malloc(sizeof(*a_star_elements) * field_h * field_w);
    for (size_t i = 0; i < field_h; ++i) {
        for (size_t j = 0; j < field_w; ++j) {
            a_star_elements[i * field_w + j].cost = HUGE_VALF;
        }
    }

    a_star_elements[start.y * field_w + start.x] = (A_Star_Element) {
        .prev           = NULL,
        .pos            = (Position) { .x = start.x, .y = start.y },
        .cost           = 0.f,
        .predicted_cost = 0.f,
        .path_len       = 1,
    };
    a_star_push(&heap, &a_star_elements[start.y * field_w + start.x]);

    while (true) {
        A_Star_Element *curr = a_star_pop(&heap);

        if (curr->pos.x == end.x && curr->pos.y == end.y) break;
        if (a_star_elements[curr->pos.y * field_w + curr->pos.x].cost < curr->cost) continue;

        for (int64_t dy = -1; dy <= 1; ++dy) {
            for (int64_t dx = -1; dx <= 1; ++dx) {
                // if (dy != 0 && dx != 0) continue;
                if (dy == 0 && dx == 0) continue;

                const int64_t next_x = curr->pos.x + dx;
                const int64_t next_y = curr->pos.y + dy;

                if (next_x < 0 || next_x >= field_w || next_y < 0 || next_y >= field_h) continue;

                float dh = fabs(field[next_y * field_w + next_x] - field[curr->pos.y * field_w + curr->pos.x]);

                const float d_cost = min_movement_cost + dh / (roads[next_y * field_w + next_x] + .1f);
                const float new_cost = curr->cost + d_cost;

                if (a_star_elements[next_y * field_w + next_x].cost <= new_cost) continue;
                a_star_elements[next_y * field_w + next_x] = (A_Star_Element) {
                    .prev           = &a_star_elements[curr->pos.y * field_w + curr->pos.x],
                    .pos            = { .x = next_x, .y = next_y },
                    .cost           = new_cost,
                    .predicted_cost = (abs(next_x - end.x) + abs(next_y - end.y)) * min_movement_cost,
                    .path_len       = curr->path_len + 1,
                };
                a_star_push(&heap, &a_star_elements[next_y * field_w + next_x]);
            }
        }
    }

    a_star_free(heap);

    Position *ret = arr_create(Position);
    arr_resize(&ret, a_star_elements[end.y * field_w + end.x].path_len);
    for (A_Star_Element *it = &a_star_elements[end.y * field_w + end.x]; it; it = it->prev) {
        ret[it->path_len-1] = it->pos;
        if (it->prev) assert(it->path_len == it->prev->path_len + 1);
    }

    free(a_star_elements);

    return ret;
}

void generate_new_target(Guy *guy) {
    Position start = guy->path[arr_len(guy->path)-1];
    Position end;

    do {
        end = (Position) {
            .x = start.x + rand64() % field_w / 3 - field_w / 6,
            .y = start.y + rand64() % field_h / 3 - field_h / 6,
        };

        if (end.x < 0)        end.x = 0;
        if (end.x >= field_w) end.x = field_w - 1;
        if (end.y < 0)        end.y = 0;
        if (end.y >= field_h) end.y = field_h - 1;
    } while (field[end.y * field_w + end.x] < WATER_TRESHOLD);

    arr_free(guy->path);
    guy->path = a_star(start, end);
    guy->path_idx = 0;
}

void generate_terrain(float *buf, size_t w, size_t h) {
    generate_noise(buf, w, h);

    for (size_t i = 0; i < h; ++i) {
        for (size_t j = 0; j < w; ++j) {
            buf[i * w + j] *= .5f;
            buf[i * w + j] += .5f;
        }
    }
}

float *make_terrain_from_image(const char *filename, size_t *buf_w, size_t *buf_h) {
    float *buf = stbi_loadf("world-1600.jpg", (int32_t*)buf_w, (int32_t*)buf_h, NULL, 1);

    float max_v = 0.0f;
    float min_v = 1.0f;

    for (size_t i = 0; i < *buf_h * *buf_w; ++i) {
        max_v = fmax(buf[i], max_v);
        min_v = fmin(buf[i], min_v);
    }

    for (size_t i = 0; i < *buf_h * *buf_w; ++i) {
        buf[i] = sqrt((buf[i] - min_v) / (max_v - min_v));
    }

    return buf;
}

void init() {

    field = make_terrain_from_image("bulgaria.jpg", &field_w, &field_h);
    // generate_terrain((float*)field, field_w, field_h);
    roads = malloc(sizeof(*roads) * field_w * field_h);

    srand64(get_timestamp());


    for (int64_t i = 0; i < GUYS_LEN; ++i) {
        guys[i].path = arr_create(Position);
        Position pos = { .x = field_w / 2, .y = field_h / 3 };
        arr_push(&guys[i].path, pos);
        guys[i].path_idx = 0;
    }
}

int64_t path_generations_this_tick;

void update_guy(Guy *guy) {
    if (guy->path_idx == arr_len(guy->path)) {
        if (path_generations_this_tick >= MAX_PATH_GENERATIONS_PER_TICK) return;
        ++path_generations_this_tick;
        generate_new_target(guy);
    }

    Position pos = guy->path[guy->path_idx];
    ++guy->path_idx;

    roads[pos.y * field_w + pos.x] += .1f;
    if (roads[pos.y * field_w + pos.x] > 1.f) roads[pos.y * field_w + pos.x] = 1.f;
}

void do_tick() {
    path_generations_this_tick = 0;
    for (int64_t i = 0; i < GUYS_LEN; ++i) {
        update_guy(&guys[i]);
    }

    for (size_t i = 0; i < field_h; ++i) {
        for (size_t j = 0; j < field_w; ++j) {
            roads[i * field_w + j] *= road_decay_rate;
        }
    }
}

void update() {
    static uint64_t prev_ts;
    if (!prev_ts) prev_ts = get_timestamp();
    float dt;
    {
        uint64_t cur_ts = get_timestamp();
        dt = (float)(cur_ts - prev_ts) / 1000000000;
        prev_ts = cur_ts;
    }

    seconds_since_last_tick += dt;

    while (seconds_since_last_tick > 1/ticks_per_second) {
        do_tick();
        seconds_since_last_tick -= 1/ticks_per_second;
    }
}

void draw() {
    {
        float min_scale = fmin(window_w / field_w, window_h / field_h);
        scale(min_scale, min_scale);
    }

    uint8_t *img = malloc(sizeof(*img) * field_h * field_w * 4);

    for (size_t i = 0; i < field_h * field_w; ++i) {
        const uint8_t g = (uint8_t)(255.f * field[i]);
        img[4 * i + 0] = g;
        img[4 * i + 1] = g;
        img[4 * i + 2] = g;
        img[4 * i + 3] = 0xff;
    }
    draw_image_buffer(img, field_w, field_h, 0, 0, field_w, field_h);

    for (size_t i = 0; i < field_h * field_w; ++i) {
        const uint8_t g = (uint8_t)(255.f * roads[i]);
        img[4 * i + 0] = 0xff;
        img[4 * i + 1] = 0xff;
        img[4 * i + 2] = 0x00;
        img[4 * i + 3] = g;
    }
    draw_image_buffer(img, field_w, field_h, 0, 0, field_w, field_h);

    free(img);

    fill_color(0xff0000);
    for (int64_t i = 0; i < GUYS_LEN; ++i) {
        Position pos = guys[i].path[arr_len(guys[i].path)-1];
        fill_rect(pos.x, pos.y, 1, 1);
    }

    fill_color(0x000000);
    for (int64_t i = 0; i < GUYS_LEN; ++i) {
        Position pos = guys[i].path[guys[i].path_idx];
        fill_rect(pos.x, pos.y, 1, 1);
    }
}

void keydown(int32_t key) {
    // printf("Keydown %d\n", key);
    switch (key) {
    case 32:
        generate_terrain((float*)field, field_w, field_h);
        break;
    case 38:
        ticks_per_second *= 2.f;
        printf("%.2f tps\n", ticks_per_second);
        break;
    case 40:
        ticks_per_second *= .5f;
        printf("%.2f tps\n", ticks_per_second);
        break;
    }
}

void keyup(int32_t key) { }

void mousedown(int32_t button) { }

void mouseup(int32_t button) {
    printf("Mouse clicked at %.0f %.0f from %.0f %.0f\n", mouse_x, mouse_y, window_w, window_h);
}

