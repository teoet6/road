#!/bin/sh

# CFLAGS='-fsanitize=address -fsanitize=undefined -fno-omit-frame-pointer --std=c11 -Wall -Werror -ggdb3'
# CFLAGS='--std=c11 -Wall -Werror -g3 -gdwarf-2'
CFLAGS='--std=c11 -Wall -Werror -O3 -march=native'

LFLAGS='-ldl -lX11 -lGL -lm'

echo gcc $CFLAGS game.c $LFLAGS -o game
gcc $CFLAGS game.c $LFLAGS -o game -fdiagnostics-color=always
